/*DATA MODEL DESIGN

Data Model Design is the step where the structure of all your service/application's data is decided, much like a blueprint is needed before building a hourse can begin.

Database:
All the collections compiled for a service/application's data.
e.g. filing cabinet

Collections:
Separate categories compiled for a service/application's data.
e.g. filing cabinet drawers
		- user collection
		- products collection
		- orders collection

Documents:
Each specific, individual record
e.g. Folder
		- specific user
		- specific product
		- specific order

Sub-documents:
Specific information about a record that is compiled similarly to a document, but is inside of a document itself.
		- user orders
		- user contact info
		- user comments

Fields:
Each record's specific information + their data type
e.g. File/folder content
		- user's name = string
		- user's age = number
		- user's email = string
		- user's password = string

When starting to model your data, the very first question you need to ask is "What kind of information will I need to save to make my app/service work?".

e.g. blog

Users:
   - ID - MongoDB ObjectID (MongoDB automatically adds an ID to all documents/records)
   - username - string
   - password - string
   - email - string
   - isAdmin - boolean

Content/Posts:
   - ID - MongoDB ObjectID (MongoDB automatically adds an ID to all documents/records)
   - title - string
   - body - string
   - datePosted - date
   - author - string/MongoDB ObjectID
   e.g.
   author: ObjectID("<ObjectID>")
   or
   author: "ObjectID("<ObjectID>")"
*/

//EMBEDDED VS. REFERENCED DATA

/*

When data is embedded, it is included as a direct part(called a subdocument) of any other data that has a relationship with it.

Users document:
{
	_id: <ObjectId1>,
	user: "123xyz",
	address: {
	street: "123 Street st",
	city: "New York"
	country: "United States"
	}
}

Address document:
{
	_id: <ObjectId2>,
	userID: <ObjectId1>
	address: {
	street: "123 Street st",
	city: "New York"
	country: "United States"
}

When to use embedded vs referenced:

Embedded vs referenced data often only has "suggestions" instead of rules. If you believe that your data is more readable/makes more sense in one way, go ahead and do so. Generally, embedded data is easier to user and understand, EXCEPT for on specific scenario:

If you expect the data that you will embed will continuously grow in size, it is safer to move that data outside the document and into its own collection, then just use references 

*/

