/*

E-commerce for Computer Components

Users:

	id - MongoDB ObjectId 
	username - string
	password - string
	email - string
	contactNo - string
	address - string
	paymentDetails - string

Products:

	id - MongoDB ObjectId 
	category - string
	sub-category - string
	name - string
	model - string
	brand - string
	price - number

Order:
	
	id - MongoDB ObjectId
	userId - MongoDB ObjectId
	productId - MongoDB ObjectId
	isCashless - boolean
	totalPrice - number
	purchasedOn - date

*/